function areaRegularNGon(n, s) {
    // n is number of sides, s is side length
    return (1 / 4) * ((n * s * s) / Math.tan(Math.PI / n))
}

module.exports = {
    areaRegularNGon
}