function areaCircle(r) {
    let circle;
    circle = Math.PI * r * r;
    return circle;
}
module.exports = {
    areaCircle
}
