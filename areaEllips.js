function areaEllips(a, b) {
    let ellips;
    ellips = Math.PI * a * b;
    return ellips;
}
module.exports = {
    areaEllips
}
