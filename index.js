const areaTriangle = require("./triangleArea");
const areaCircle = require("./areaCircle");
const areaEllips = require("./areaEllips");
const areaWidetube = require("./areaTube");
const areaVolumetube = require("./areaTube");
const areaKite = require("./areaKite");
const areaRegularNg = require("./areaRegularNGon.js");
const areaTriangleside = require("./areaTriangleSide");
const rhombusArea = require("./areaRhombus");
const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});
let i;

console.log("Welcome to calculator : \n");
console.log("1. Triangle Area");
console.log("2. Circle Area");
console.log("3. Ellips Area");
console.log("4. Wide Tube Area");
console.log("5. Volume Tube Area");
console.log("6. Kite Area");
console.log("7. Regular Area");
console.log("8. Triangle Side Area");
console.log("9. Rhombus Area");

rl.question("\n Choose by number ", (i) => {
  if (i == 1) {
    console.log("Welcome to Count Triangle Area \n");
    rl.question("value of base = ", (base) => {
      rl.question("value of height =  ", (heigth) => {
        console.log(
          `Rectangle area = ${areaTriangle.rectangleArea(base, heigth)}`
        );
        rl.close();
      });
    });
  } else if (i == 2) {
    console.log("Welcome to Count Circle Area \n");
    rl.question("value of radius = ", (r) => {
      console.log(`Circle area = ${areaCircle.areaCircle(r)}`);
      rl.close();
    });
  } else if (i == 3) {
    console.log("Welcome to Count Ellips Area \n");
    rl.question("value of a = ", (a) => {
      rl.question("value of b =  ", (b) => {
        console.log(`Ellips Area = ${areaEllips.areaEllips(a, b)}`);
        rl.close();
      });
    });
  } else if (i == 4) {
    console.log("Welcome to Count Wide Tube area \n");
    rl.question("value of base area = ", (la) => {
      rl.question("value of blanked area =  ", (lb) => {
        console.log(`Wide Tube Area = ${areaWidetube.areaWideTube(la, lb)}`);
        rl.close();
      });
    });
  } else if (i == 5) {
    console.log("Welcome to Count Volume Tube Area \n");
    rl.question("value of base radius = ", (r) => {
      rl.question("value of blanked height =  ", (t) => {
        console.log(
          `Volume Tube Area = ${areaVolumetube.areaVolumeTube(r, t)}`
        );
        rl.close();
      });
    });
  } else if (i == 6) {
    console.log("Welcome to Count Kite Area \n");
    rl.question("value of base d1 = ", (d1) => {
      rl.question("value of d2 =  ", (d2) => {
        console.log(`Kite Area = ${areaKite.areaKite(d1, d2)}`);
        rl.close();
      });
    });
  } else if (i == 7) {
    console.log("Welcome to Count Regular Area \n");
    rl.question("value of side = ", (n) => {
      rl.question("value of lenght =  ", (s) => {
        console.log(`Regular Area = ${areaRegularNg.areaRegularNGon(n, s)}`);
        rl.close();
      });
    });
  } else if (i == 8) {
    console.log("Welcome to Count Triangle Side Area \n");
    rl.question("value of a = ", (a) => {
      rl.question("value of b =  ", (b) => {
        rl.question("value of c =  ", (c) => {
          console.log(
            `Area Triangle Side = ${areaTriangleside.areaTriangleSide(a, b, c)}`
          );
          rl.close();
        });
      });
    });
  } else if (i == 9) {
    console.log("Welcome to Count Rhombus Area \n");
    rl.question("value of diagonal 1 = ", (diagonal1) => {
      rl.question("value of diagonal 2 =  ", (diagonal2) => {
        console.log(
          `Rhombus Area = ${rhombusArea.areaRhombus(diagonal1, diagonal2)}`
        );
        rl.close();
      });
    });
  } else {
    console.log("Please input the right number");
  }
});
