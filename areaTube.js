//wide
function areaWideTube (la, lb){
    // la = base area
    // lb = blanked area
    let wide;
    wide = 2 * la * lb;
    return wide;
}
module.exports = {
    areaWideTube
}

//Volume
function areaVolumeTube (r, t){
    //r = jari jari
    //t = tall
    let Volume;
    Volume = Math.PI * r * r * t;
    return Volume

}
module.exports = {
    areaVolumeTube
}
