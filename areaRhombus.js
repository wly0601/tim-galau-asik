function areaRhombus (diagonal1, diagonal2) {
    let area;
    area = 1/2 * diagonal1 * diagonal2;
    return area;
}
module.exports = {
    areaRhombus
}
